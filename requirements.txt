numpy>=1.24.0
tensorflow==2.13.0  
torch>=2.0.0
torchvision>=0.15.0
pillow>=9.5.0
notebook>=7.0.0