stages:
  - build
  - test
  - train
  - publish

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
    rules:
      - if: $CI_PIPELINE_SOURCE == "schedule"

variables:
  IMAGE_PATH: "${CI_REGISTRY_IMAGE}:latest"
  GIT_STRATEGY: fetch
  MODEL_NAME: "Handwritten-Digits-Recognizer"
  VERSION: "0.0.1"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.pip-cache"

cache:
  paths:
    - .pip-cache/

.train_output: &train_output
  artifacts:
    paths:
      - training_output.log
      - mnist.h5
    expose_as: 'trained model'

train:
  stage: train
  tags:
    - saas-linux-medium-amd64-gpu-standard
  image: $IMAGE_PATH
  variables:
    NVIDIA_VISIBLE_DEVICES: all
    NVIDIA_DRIVER_CAPABILITIES: compute,utility
  script:
    - python train_digit_recognizer.py > training_output.log
  <<: *train_output

publish:
  stage: publish
  when: manual
  image: curlimages/curl:latest
  dependencies:
    - train
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  script:
    - |
      # Check if model version already exists
      if ! curl --fail --silent --output /dev/null \
        --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/ml/models/${MODEL_NAME}/versions/${VERSION}"; then
        
        echo "Creating new model version..."
        # Create model version
        if ! curl --fail --request POST \
          --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
          --header "Content-Type: application/json" \
          --data "{
            \"name\": \"${MODEL_NAME}\",
            \"version\": \"${VERSION}\"
          }" \
          "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/ml/models"; then
          echo "Failed to create model version"
          exit 1
        fi
        
        echo "Uploading model file..."
        # Upload model file
        if ! curl --fail --request POST \
          --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
          --form "file=@mnist.h5" \
          "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/ml/models/${MODEL_NAME}/versions/${VERSION}/metrics"; then
          echo "Failed to upload model file"
          exit 1
        fi
      else
        echo "Model version ${VERSION} already exists"
        exit 0
      fi
  needs: ["train"]

container_scanning:
  variables:
    CS_IMAGE: $IMAGE_PATH
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  needs: ["docker-build"]
  allow_failure: true

gemnasium-python-dependency_scanning:
  tags:
    - saas-linux-large-amd64
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  needs: ["docker-build"]
  allow_failure: true

.set_image_version:
  before_script:
    - export IMAGE_VERSION="${VERSION}-$(date '+%Y%m%d')"

docker-build:
  stage: build
  extends: .set_image_version
  tags:
    - saas-linux-large-amd64
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - echo "Building image version ${IMAGE_VERSION}"
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${IMAGE_PATH}"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
      --destination "${CI_REGISTRY_IMAGE}:${IMAGE_VERSION}"
      --cache
      --cache-copy-layers
      --cache-run-layers=true
      --cache-ttl=24h
      --cache-repo="${CI_REGISTRY_IMAGE}/cache"
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"

test-image:
  stage: test
  tags:
    - saas-linux-medium-amd64-gpu-standard
  image: $IMAGE_PATH
  script:
    - nvidia-smi
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  needs: ["docker-build"]
  allow_failure: true

publish-container:
  stage: publish
  extends: .set_image_version
  image: docker:latest
  tags:
    - saas-linux-large-amd64
  services:
    - docker:dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - |
      docker pull $IMAGE_PATH
      docker tag $IMAGE_PATH $CI_REGISTRY_IMAGE:$IMAGE_VERSION
      docker push $CI_REGISTRY_IMAGE:$IMAGE_VERSION

      # Update latest tag and cleanup
      docker push $IMAGE_PATH
      docker image rm $IMAGE_PATH
      docker system prune --volumes -f
  needs: ["docker-build", "test-image"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"